const links = [];

//===============================================================
//======================  RENDER LINKS  =========================
//===============================================================
function renderLinks() {
	main.innerHTML = '';
	links.forEach((ele) => {
		createA(ele);
	});
}

//===============================================================
//========================  CREATE A TAG  =======================
//===============================================================

function createA(ele) {
  const a = document.createElement('a');
  const list = document.getElementById('main');
	a.setAttribute('href', 'todo_container');
	a.innerHTML = ele;
	list.appendChild(a);
}
//===============================================================
//=====================  CREATE LINK  ===========================
//===============================================================
function createLink() {
	const inputVal = document.getElementById('myInput').value;
	links.push(inputVal);
	document.getElementById('myInput').value = '';
	renderLinks();
}
